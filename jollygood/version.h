#if 0
# this file is valid C, Makefile and shell
# this is the only file required to update when changing the version

# https://semver.org/
VERSION_MAJOR=1
VERSION_MINOR=7
VERSION_PATCH=6
#endif

#ifndef JG_VERSION
#define JG_VERSION "1.7.6"
#endif
